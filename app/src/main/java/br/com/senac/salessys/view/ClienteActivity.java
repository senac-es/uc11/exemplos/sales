package br.com.senac.salessys.view;

import android.content.Intent;
import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.salessys.R;
import br.com.senac.salessys.model.Cliente;

public class ClienteActivity extends AppCompatActivity {

    public static final String CLIENTE = "cliente" ;

    private EditText txtNome;
    private Spinner spinnerEstados ;
    private Spinner spinnerCidades ;
    private EditText txtProfissao;
    private EditText txtTelefone;
    private EditText txtEmail;
    private EditText txtNomeEmpresa ;
    private EditText txtObservacao;
    private Button btnSalvar;
    private Button btnCancelar;

    private Cliente cliente ;

    private List<String> listaEstados = new ArrayList<>();

    private void init(){
        listaEstados.add("Acre");
        listaEstados.add("Alagoas");
        listaEstados.add("Amazonas");
        listaEstados.add("Bahia");
        listaEstados.add("Ceará");
        listaEstados.add("Distrito Federal");
        listaEstados.add("Espírito Santo");
        listaEstados.add("Goiás");
        listaEstados.add("Maranhão");
        listaEstados.add("Mato Grosso");
        listaEstados.add("Mato Grosso do Sul");
        listaEstados.add("Minas Gerais");
        listaEstados.add("Pará");
        listaEstados.add("Paraíba");
        listaEstados.add("Paraná");
        listaEstados.add("Pernambuco");
        listaEstados.add("Piauí");
        listaEstados.add("Rio de Janeiro");
        listaEstados.add("Rio Grande do Norte");
        listaEstados.add("Rio Grande do Sul");
        listaEstados.add("Rondônia");
        listaEstados.add("Roraima");
        listaEstados.add("Santa Catarina");
        listaEstados.add("São Paulo");
        listaEstados.add("Sergipe");
        listaEstados.add("Tocantins");
    }

    private void preencherObjetoNoFormulario(Cliente cliente){

        txtNome.setText(cliente.getNomeCompleto());


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        init()  ;

        txtNome = findViewById(R.id.txtNomeCompleto) ;
        spinnerEstados = findViewById(R.id.spnEstados) ;
        spinnerCidades = findViewById(R.id.spnCidade);
        final EditText txtProfissao= findViewById(R.id.txtProfissao) ;
        txtTelefone= findViewById(R.id.txtTelefone) ;
        txtEmail= findViewById(R.id.txtEmail) ;
        txtObservacao= findViewById(R.id.txtObservacoes) ;
        txtNomeEmpresa= findViewById(R.id.txtNomeEmpresa) ;
        btnSalvar = findViewById(R.id.btnSalvar);
        btnCancelar= findViewById(R.id.btnCancelar);



        ArrayAdapter<String> adapterEstado
                = new ArrayAdapter<String>(
                this ,
                android.R.layout.simple_spinner_item ,
                listaEstados
        );
        spinnerEstados.setAdapter(adapterEstado);


        ArrayAdapter<CharSequence> adapterCidade = ArrayAdapter.createFromResource(
                this ,
                R.array.listaCidades ,
                android.R.layout.simple_spinner_item);

        spinnerCidades.setAdapter(adapterCidade);


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome = txtNome.getText().toString();
                String uf = (String)spinnerEstados.getSelectedItem();
                String cidade = (String)spinnerCidades.getSelectedItem() ;
                String profissao = txtProfissao.getText().toString() ;
                String email = txtEmail.getText().toString() ;
                String observacao = txtObservacao.getText().toString() ;
                String telefone = txtTelefone.getText().toString() ;
                String nomeEmpresa = txtNomeEmpresa.getText().toString() ;

                if(cliente == null) {
                    cliente = new Cliente(0, nome,
                            cidade,
                            uf, profissao, nomeEmpresa, telefone, email, observacao);
                }

                Intent intent = new Intent();
                intent.putExtra(CLIENTE , cliente);

                setResult(RESULT_OK , intent);
                finish();


               // Toast.makeText(ClienteActivity.this, "Salvo com sucesso", Toast.LENGTH_SHORT).show();


            }
        });



        Intent intent = getIntent() ;

        cliente = (Cliente) intent.getSerializableExtra(MainActivity.CLIENTE) ;

        if(cliente != null ){
            preencherObjetoNoFormulario(cliente);
        }





























    }
}
