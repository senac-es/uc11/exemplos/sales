package br.com.senac.salessys.model;


import java.io.Serializable;










public class Cliente implements Serializable {

    private  int id ;
    private String nomeCompleto ;
    private String cidade ;
    private String uf ;
    private String profissao ;
    private String nomeEmpresa ;
    private String telefone ;
    private String email ;
    private String Observacoes ;

    public Cliente() {
    }

    public Cliente(int id, String nomeCompleto, String cidade, String uf, String profissao, String nomeEmpresa, String telefone, String email, String observacoes) {
        this.id = id;
        this.nomeCompleto = nomeCompleto;
        this.cidade = cidade;
        this.uf = uf;
        this.profissao = profissao;
        this.nomeEmpresa = nomeEmpresa;
        this.telefone = telefone;
        this.email = email;
        Observacoes = observacoes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacoes() {
        return Observacoes;
    }

    public void setObservacoes(String observacoes) {
        Observacoes = observacoes;
    }


    @Override
    public String toString() {
        return this.nomeCompleto;
    }
}
